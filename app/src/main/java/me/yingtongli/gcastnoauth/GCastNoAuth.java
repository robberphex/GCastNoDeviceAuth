package me.yingtongli.gcastnoauth;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

import static de.robv.android.xposed.XposedHelpers.findAndHookMethod;

public class GCastNoAuth implements IXposedHookLoadPackage {
    @Override
    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam lpparam) throws Throwable {
        if (!lpparam.packageName.equals("com.google.android.gms")) {
            return;
        }

        XposedBridge.log("[GCastNoAuth] Loaded into com.google.android.gms");

        // Hook debug logging function to show logs
        /*
        findAndHookMethod("qco", lpparam.classLoader, "a", String.class, "[Ljava.lang.Object;", new XC_MethodHook() {
            @Override
            protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                String logMessage = (String) callMethod(param.thisObject, "e", new Class<?>[] {String.class, Class.forName("[Ljava.lang.Object;")}, param.args);
                XposedBridge.log("[GCastNoAuth] [Debug] " + logMessage);
            }
        });
        */

        // Hook certificate check function
        /*
        findAndHookMethod("ptd", lpparam.classLoader, "a", "[B", new XC_MethodHook() {
            @Override
            protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                XposedBridge.log("[GCastNoAuth] beforeHookedMethod");
                byte[] arg = (byte[]) param.args[0];
                XposedBridge.log(Arrays.toString(arg));
            }

            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                XposedBridge.log("[GCastNoAuth] afterHookedMethod");
                if (param.hasThrowable()) {
                    XposedBridge.log("[GCastNoAuth] An exception was raised");
                    XposedBridge.log(param.getThrowable());
                } else {
                    XposedBridge.log("[GCastNoAuth] No throwable");
                }
            }
        });*/

        // Hook certificate post-check function
        findAndHookMethod("ptd", lpparam.classLoader, "a", int.class, Exception.class, new XC_MethodHook() {
            @Override
            protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                XposedBridge.log("[GCastNoAuth] beforeHookedMethod in ptd.a(int,Exception)");
                // Force success
                param.args[0] = 0;
            }
        });
    }
}
